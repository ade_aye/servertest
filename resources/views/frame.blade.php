<!DOCTYPE html>
<html ng-app="myApp">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>Lazada ku</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/ng-tag/ng-tags-input.css">
  <link rel="stylesheet" href="assets/css/skins/skin-green.min.css">
  <link rel="stylesheet" href="assets/css/swal/sweetalert.css">

</head>
<body class="hold-transition skin-green sidebar-collapse sidebar-mini">
  <div class="wrapper">
    @include('header')

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      @include('side_nav')
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <div ng-view></div>
    </div><!-- /.content-wrapper -->
  </div>
  <script src="assets/js/jquery/jQuery-2.1.4.min.js"></script>
  <script src="assets/js/jqueryui/jquery-ui.min.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <script type="text/javascript">
    $(window).bind('beforeunload', function(){
      return 'Are you sure you want to leave?';
    });
  </script>
  <script src="assets/js/bootstrap/bootstrap.min.js"></script>
  <script src="assets/js/adminlte/app.min.js"></script>
  
  <script src="assets/js/angular/angular.min.js"></script>
  <script src="assets/js/angular/angular-route.min.js"></script>
  <script src="assets/js/angular/angular-sanitize.min.js"></script>
  <script src="assets/js/swal/sweetalert.min.js"></script>
  <script src="assets/js/tinymce/tinymce.min.js"></script>
  <script src="assets/js/angularui-tinymce/tinymce.js"></script>
  <script src="assets/js/ng-tag/ng-tags-input.js"></script>

  <script src="assets/js/app.js"></script>
  
  <script src="assets/js/angular-component/config.js"></script>
  <script src="assets/js/angular-component/service.js"></script>
  <script src="assets/js/angular-component/Lazada/add_product_controller.js"></script>
  <script src="assets/js/angular-component/filter.js"></script>
  <script src="assets/js/angular-component/directive.js"></script>
  <script src=""></script>
</body>
</html>