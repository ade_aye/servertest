<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="assets_admin/img/avatar.png" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p></p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="#addproduct">
                <i class="fa fa-child"></i> <span>Add Product</span>
              </a>
            </li>
            <li>
              <a href="#list">
                <i class="fa fa-child"></i> <span>List Product</span>
              </a>
            </li>
            <li>
              <a href="#addownproduct">
                <i class="fa fa-child"></i> <span>Add own Product</span>
              </a>
            </li>
          </ul>
        </section>