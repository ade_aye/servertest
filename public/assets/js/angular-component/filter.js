'use strict';

/* Filters */

angular.module('myApp.filters', [])

	// Find Specific Segment in Current Url
	// ==================================================
	
		.filter('_uriseg', function($location, globals) {
			return function(segment) {
				// Get URI and remove the domain base url global var
				var query = $location.absUrl().replace(globals.baseUrl,"");
				// To obj
				var data = query.split("/");
				// Return segment *segments are 1,2,3 keys are 0,1,2
				if(data[segment-1]) {
					return data[segment-1];
				}
				return false;
			};
		})

		.filter('range', function() {
		  return function(input, total) {
		    total = parseInt(total);

		    for (var i=0; i<total; i++) {
		      input.push(i);
		    }

		    return input;
		  };
		});
;
