'use strict';

/* Controllers */

angular.module('myApp.controllers', [])


    // Home
    // ==================================================
    

    .controller(
      'add_product',
      [
      '$rootScope', '$scope', '$http', 'globals', '$location',
      function($rootScope, $scope, $http, globals, $location) {

      	var api_url = "http://" + window.location.host + "/api/"

      $scope.category_list = [];
      $scope.grab_done = true;
      $scope.showPhoneOption = false;
      $scope.lazada_phone = '';
      $scope.tags= [];

      $scope.tinymceOptions = {
        // plugins : 'advlist autolink link image lists charmap print preview'
        plugins: 'link image code',
          toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | bullist numlist'
      };

      $scope.getCategory = function(data)
      {
        console.log(data);
        if (data == 14727 || data == 14728 || data == 14735 || data == 14736) 
        {
          $scope.showPhoneOption = true
        }
        else
        {
          $scope.showPhoneOption = false
        }
      }

      $scope.find_phone = function()
          {
            $http.get(api_url + 'ade/lazada/searchphonelist?keyword='+$scope.phone_keyword)
            .success(function(response)
            {
              $scope.phone_list = response.data;
            })
          }

      $scope.fetchUrl = function()
      {
          $scope.grab_done = false;

          var submit =
          {
            url : $scope.url
          }

          console.log(submit)

          $http({
                  method  : 'POST',
                  url   : api_url+'ade/lazada/grabjaknot',
                  data  : submit
                })
          .success(function(response)
          {
            $scope.grab_done = true;

            $scope.data_grab = response.data;
            $scope.product_name = response.data.product_name;
            console.log($scope.data_grab)
          })
      }

      $scope.find_category = function()
      {
        console.log('asd');
          $http.get(api_url + 'ade/lazada/searchcategory?keyword='+$scope.category_keyword)
          .success(function(response)
          {
            $scope.category_list = response.data;
          })
      }

      $scope.find_brand = function()
      {
        console.log('asd');
          $http.get(api_url + 'ade/lazada/searchbrand?keyword='+$scope.brand_keyword)
          .success(function(response)
          {
            $scope.brand_list = response.data;
          })
      }

      $scope.upload = function()
      {
        // console.log('sedang implemen keyword')
        // return;
        
        $('#myModal').modal({keyboard: false,show:true});

        var modalPrice = parseInt($scope.data_grab.discount_price);
        var profit = 5000;
        var tax = parseInt($scope.price_after)*1.3/100;
        let PPN = tax*0.1;
        var onkir = (Math.ceil(parseFloat($scope.data_grab.weight)) !== 1 ? 14000 : 7000);
        var discount = 15*parseInt($scope.price_after)/100;


        var itemPrice = parseInt($scope.price_after) - profit - tax - onkir - discount - PPN;
        if (itemPrice < modalPrice) {
          $('#myModal').modal('hide');
          var suggest = modalPrice+profit+tax+onkir+discount+PPN;
          swal("Rugi!", 'set harga minimal Rp '+ suggest , "error");

          return;
        }

        var keyword = $scope.tags.map(function(tag){
          return tag.text;
        })

        var data =
        {
          url           : $scope.url,
          product_name  : $scope.product_name,
          brand         : $scope.lazada_brand,
          category      : $scope.lazada_category,
          color         : $scope.color,
          highlight     : $scope.highlight,
          sku           : $scope.data_grab.sku,
          description   : $scope.data_grab.description,
          inbox         : $scope.data_grab.inbox,
          prefix_sku    : $scope.prefix_sku,
          price_before  : $scope.price_before,
          price_after   : $scope.price_after,
          warranty      : $scope.data_grab.warranty,
          stock         : $scope.data_grab.stock,
          weight        : $scope.data_grab.weight,
          length        : $scope.length,
          height        : $scope.height,
          width         : $scope.width,
          images        : $scope.data_grab.image,
          main_image    : $scope.main_image_index,
          phone         : $scope.lazada_phone,
          lazadaUserId  : 1,
          keyword       : keyword,
          info          : $scope.data_grab.additional_info,
        }
        console.log(data);

        $http({
                  method  : 'POST',
                  url   : api_url+'ade/lazada/uploadproceed',
                  data  : data
                })
          .success(function(response)
          {
            $('#myModal').modal('hide');
            if (response.success) 
            {
              swal("Sukses!", response.message, "success");
            }
            else
            {
              swal("Gagal!", response.message, "error");
            }
          })

      }


      //end
      }])

// list controller
    .controller(
      'list_product',
      [
      '$rootScope', '$scope', '$http', 'globals', '$location',
      function($rootScope, $scope, $http, globals, $location) {

        var api_url = "http://" + window.location.host + "/api/";

        $scope.editSalePriceValue = '';
        $scope.editPriceValue = '';
        $scope.editButtonDisable=false;
        $scope.editButtonText='Save Change';

        function getProductList(page)
        {
          $scope.currentPage = page;
          $http.get(api_url + 'ade/lazada/listproduct?page='+page)
            .success(function(response)
            {
              $scope.totalPage = response.data.last_page;
              $scope.list = response.data.data;
            });
        }

        getProductList(1);

        $scope.changePage = function(nextPage)
        {
          getProductList(nextPage);
        }

        $scope.toIdr = function(nominal)
        {
          return ('Rp '+(parseInt(nominal)/1000).toFixed(3));
        }

        $scope.profit = function(sell,buy)
        {
          var dropship = 5000;
          var diffOnkirLazada = 4000 //assume kena 2kg -> perkilo beda 2000
          var buy = parseInt(buy)+dropship+diffOnkirLazada;

          var result = $scope.toIdr(parseInt(sell)-buy);

          return ('± '+result);
        }

        $scope.jaknotNormalPrice= function(price)
        {
          return(parseInt(price)-20000);
        }

        $scope.detail = function(product)
        {
          $scope.product_detail = product;
        }

        $scope.update = function(product)
        {
          $scope.editButtonText='Processing...';
          $scope.editButtonDisable=true;

          if ($scope.editPriceValue == '') {
            $scope.editPriceValue = product.detail.Price;
          }
          if ($scope.editSalePriceValue == '') {
            swal("Peringatan!", 'Harga jual harus diisi', "warning");
            $scope.editButtonText='Save Change';
            $scope.editButtonDisable=false;
            return;
          }

          if (parseInt($scope.editPriceValue) < parseInt($scope.editSalePriceValue)) {
            // console.log($scope.editPriceValue<$scope.editSalePriceValue);
            swal("Peringatan!", 'Harga jual harus lebih kecil dari harga normal', "warning");
            $scope.editButtonText='Save Change';
            $scope.editButtonDisable=false;
            return;
          }

          if (parseInt($scope.editSalePriceValue) <= recommendPrice(product)) {
            swal("Peringatan!", 'Tidak untung, disarankan set harga ke '+ $scope.toIdr(recommendPrice(product)), "warning");
            $scope.editButtonText='Save Change';
            $scope.editButtonDisable=false;
            return; 
          }
          
          var submit={
            price : parseInt($scope.editPriceValue),
            salePrice : parseInt($scope.editSalePriceValue),
            sku : product.sku,
            itemDetailId:product.detail.id
          }

          // console.log(recommendPrice(product));
          // return;

          $http({
                  method  : 'POST',
                  url   : api_url+'ade/update/updatelazadaprice',
                  data  : submit
                })
          .success(function(response)
          {
            if (response.success) {
              $scope.editButtonText='Save Change';
              $scope.editButtonDisable=false;
              $scope.editSalePriceValue = '';
              $scope.editPriceValue = '';
              swal("Sukses!", response.message, "success");

              $('#detail_item').modal('hide');

              getProductList($scope.currentPage);
            }
            else
            {
              $scope.editSalePriceValue = '';
              $scope.editPriceValue = '';
              $scope.editButtonText='Save Change';
              $scope.editButtonDisable=false;
              swal("Peringatan!", response.message, "warning");
            }
          })
        }

        function discount(nominal)
        {
          return (15/100*parseInt(nominal));
        }

        function recommendPrice(product)
        {
          let diffOnkir = Math.ceil(parseFloat(product.jaknote.weight))*7000;
          let dropshipFee = 5000;
          let profit = 3000;
          let paymentFee = 0.018*(parseInt($scope.jaknotNormalPrice(product.jaknote.discount_price))+diffOnkir+dropshipFee+profit); 
          // payment fee increase from 1,3%  to 1,8%
          let PPN = 0.1*paymentFee;

            return (parseInt($scope.jaknotNormalPrice(product.jaknote.discount_price))+diffOnkir+dropshipFee+profit+paymentFee+PPN+parseInt(discount($scope.editSalePriceValue)));
        }




      //end
      }])

// list controller
    .controller(
      'add_own_product',
      [
      '$rootScope', '$scope', '$http', 'globals', '$location',
      function($rootScope, $scope, $http, globals, $location) {

      window.scope = $scope;
      var api_url = "http://" + window.location.host + "/api/";
      $scope.theFile=[];

      $scope.setFile = function(index,element) {
          $scope.$apply(function() {
              $scope.theFile[index] = element.files[0];
              console.log($scope.theFile[0]);
              $scope.uploadImage(index,$scope.theFile[index]);
          });
      };

      $scope.uploadImage = function(index,image)
      {
        var fd = new FormData();
          fd.append('image', image);
          $http.post(api_url + "ade/lazada/uploadimage", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
          })
          .success(function(response){
            if(response.success == false) {
          // $('#confirm_form').removeClass('loading');
          swal("Gagal!", response.message, "error");
          return;
        } else {
          swal("Sukses!", response.message, "success");

        }
          })
          .error(function(response){
            alert('Something went wrong.')
          });
      }


      //end
      }])
;