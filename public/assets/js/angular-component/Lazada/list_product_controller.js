'use strict';

/* Controllers */

angular.module('myApp.controllers', [])


    // Home
    // ==================================================
    

    .controller(
      'list_product',
      [
      '$rootScope', '$scope', '$http', 'globals', '$location',
      function($rootScope, $scope, $http, globals, $location) {

      	var api_url = "http://" + window.location.host + "/api/"

        $http.get(api_url + 'ade/lazada/listproduct')
            .success(function(response)
            {
              $scope.list = response.data;
            })

      //end
      }])

;