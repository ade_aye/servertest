'use strict';

// Declare app level module which depends on filters, and services

// MODULE
// ==================================================

  angular.module('myApp', [
    'ngRoute',
    'ngSanitize',
    'ui.tinymce',
    'ngTagsInput',
    'myApp.filters',
    'myApp.services',
    'myApp.config',
    'myApp.directives',
    'myApp.controllers',
  ])

// end module

//config

.config(['$routeProvider', '$locationProvider', '$httpProvider',
    function($routeProvider, $locationProvider, $httpProvider)
  {
      
      $routeProvider.when('/',
      {
        templateUrl: 'partials/dashboard.html'
      });

      $routeProvider.when('/addproduct',
      {
        templateUrl: 'partials/lazada/add_product.html',
        controller: 'add_product'
      });

      $routeProvider.when('/list',
      {
        templateUrl: 'partials/lazada/list_product.html',
        controller: 'list_product'
      });

      $routeProvider.when('/addownproduct',
      {
        templateUrl: 'partials/lazada/add_own_product.html',
        controller: 'add_own_product'
      });

  }])


.run(function($rootScope, $location, globals) {

      $rootScope.goToUrl = function (url)
      {
        $location.path(url);
      }

      $rootScope.globals = globals;

  })
  
  ;