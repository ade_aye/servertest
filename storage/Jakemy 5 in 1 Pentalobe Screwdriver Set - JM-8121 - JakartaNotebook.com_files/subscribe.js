function popupSubscribeClearMessage(){$('#popup-subscribe div.notif').removeClass('is-success is-fail').empty();}
function popupSubscribeShowMessage(type,message){var icoClass=(type==='success'?'check':'cross');popupSubscribeClearMessage();$('#popup-subscribe div.notif').addClass('is-'+type).html('<i class="ir ico-'+icoClass+'"></i>'+message);if(type==='fail')return false;return true;}
function popupSubscribeGenerateCaptcha(email){General.disable('popup-subscribe-button');Captcha.create('popup-subscribe-captcha-container',function(){General.enable('popup-subscribe-button');if($('#footer-subscribe-input').val()==''){$('#popup-subscribe-input').focus();}
else{$('#popup-subscribe-captcha-input').focus();}});}
function ssp(){$('#footer-subscribe-a').click();return false;}
function subscribe(){var footerEl=$('#footer-subscribe-input');var el=$('#popup-subscribe-input');var email=footerEl.val();el.val(email);popupSubscribeGenerateCaptcha();return false;}
$(function(){$('#footer-subscribe-a').magnificPopup({type:'inline'});$('#popup-subscribe-form').ajaxForm({beforeSubmit:function(arr,form,options){if($('#popup-subscribe-input').val()===''){$('#popup-subscribe-input').focus();return popupSubscribeShowMessage('fail','Email harus diisi.');}
else if($('#popup-subscribe input[name="captcha_response"]').val()===''){$('#popup-subscribe input[name="captcha_response"]').focus();return popupSubscribeShowMessage('fail','Captcha harus diisi.');}
else if($('#popup-subscribe input[name="captcha_response"]').val().length!=$('#popup-subscribe input[name="captcha_response"]').attr('maxlength')){$('#popup-remind input[name="captcha_response"]').focus();return popupSubscribeShowMessage('fail','Jumlah huruf captcha tidak sesuai.');}
$('#popup-subscribe div.notif').html('<img id="loadProduct" alt="Loading..." title="Loading" src="'+base_url+'images/loading.gif" />');General.disable('popup-subscribe-button');return true;},clearForm:false,dataType:'json',error:function(){popupSubscribeShowMessage('fail','Maaf terjadi kesalahan pada system kami, silahkan coba beberapa saat lagi.');General.enable('popup-subscribe-button');popupSubscribeGenerateCaptcha();},resetForm:false,success:function(result){if(result!==null){if(result.success){popupSubscribeShowMessage('success',result.message);$('#footer-subscribe-input').val('');}
else
{popupSubscribeShowMessage('fail',result.message);popupSubscribeGenerateCaptcha();}}
else
{popupSubscribeShowMessage('fail','Maaf terjadi kesalahan pada sistem kami, silahkan coba beberapa saat lagi.');popupSubscribeGenerateCaptcha();}
General.enable('popup-subscribe-button');}});});