function review_finish_submit()
{$('#writeReview button[type="submit"]').removeAttr('disabled').removeClass('isDisabled');}
function review_start_submit()
{$('#writeReview button[type="submit"]').attr('disabled','disabled').addClass('isDisabled');}
function review_reloading_captcha()
{review_start_submit();Captcha.create("captcha-review",function()
{review_finish_submit();});return false;}
function review_clear_message()
{$('#writeReview div.notif').removeClass('is-success is-fail').html('');}
function review_show_message(type,message)
{var icon_class=(type==='success'?'check':'cross');review_clear_message();$('#writeReview div.notif').addClass('is-'+type).html('<i class="ir ico-'+icon_class+'"></i>'+message);if(type==='fail')return false;return true;}
function voteReview(a,b)
{Ajax.request('products','voteReview',{id:a,ans:b},'#span-vote-review'+a,'',function(result){if(result==true){$('#span-vote-review-'+a).html('You rated this review as <b>'+(b==1?"helpful":"unhelpful")+'</b>');}
else{General.showMessage('#span-vote-review-'+a,'Failed, please try again later','fail');}},Ajax.failed);return false;}
var page_review=2;$(function(){$('.detailGallery').bxSlider({slideWidth:200,buildPager:function(slideIndex){return'<img src="'+thumb_image[slideIndex]+'" height="40" />';}});setTimeout(function(){$('.baloon.purple').addClass('active');setTimeout(function(){$('.baloon.orange').addClass('active');},20000)},5000);$('.postReview').click(function(){$('html,body').animate({scrollTop:$("#customer-reviews").offset().top},'slow');if($('#writeReview').hasClass('visuallyhidden'))
{review_reloading_captcha();$('#writeReview').removeClass('visuallyhidden');$('#writeReview input[name="title"]').focus();}
return false;});$('#btn-load-review').click(function(){$.ajax({type:"GET",url:$(this).attr('href')+'/'+page_review,dataType:"json",success:function(result){if(result!==null){$('.detailReview').append(result.html);if(result.more==true){page_review++;}
else{$('#btn-load-review').addClass('visuallyhidden');}}},error:function(jqXHR,textStatus,errorThrown){alert('Maaf terjadi kesalahan fatal pada sistem kami : Gagal menampilkan review.');}});return false;});$('#writeReview form').ajaxForm({beforeSubmit:function(arr,form,options){if($('#writeReview form input[name="title"]').val()==='')
{$('#writeReview form input[name="title"]').focus();return review_show_message('fail','Anda belum mengisi judul review.');}
else if($('#writeReview form input[type="textarea"]').val()==='')
{$('#writeReview form input[type="textarea"]').focus();return review_show_message('fail','Anda belum mengisi review.');}
else if($('#writeReview form input[type="radio"]').val()==='')
{return review_show_message('fail','Anda belum memilih rating.');}
else if($('#writeReview form input[name="captcha_response"]').val()==='')
{$('#writeReview form input[name="captcha_response"]').focus();return review_show_message('fail','Anda belum mengisi captcha.');}
else if($('#writeReview form input[name="captcha_response"]').val().length!=$('#writeReview form input[name="captcha_response"]').attr('maxlength'))
{$('#writeReview form input[name="captcha-response"]').focus();return review_show_message('fail','Jumlah huruf captcha tidak sesuai.');}
review_start_submit();return true;},clearForm:false,resetForm:false,dataType:'json',error:function(){review_show_message('fail','Maaf terjadi kesalahan fatal pada system kami, silahkan coba lagi beberapa saat lagi.');review_finish_submit();review_reloading_captcha();},success:function(result){if(result!==null){if(result.success){review_show_message('success',result.message);review_reloading_captcha();$('#writeReview form')[0].reset();}
else
{review_show_message('fail',result.message);review_reloading_captcha();}}
else
{review_show_message('fail','Maaf terjadi kesalahan fatal pada sistem kami, silahkan coba lagi beberapa saat lagi.');review_reloading_captcha();}
review_finish_submit();}});$('.ccRow .ccCol select.customSelect').change(function(){index_installment=$(this).attr('id').substr(15);index_tenor=$(this).val();$('div.ccTable div.ccRow:eq('+(parseInt(index_installment))+') div.ccCol:eq(3)').html(installment[index_installment].Tenor[index_tenor].Month+' x '+installment[index_installment].Tenor[index_tenor].Pay);$('div.ccTable div.ccRow:eq('+(parseInt(index_installment))+') div.ccCol:eq(4)').html('Rp. '+installment[index_installment].Tenor[index_tenor].Minimum+',-');$('div.ccTable div.ccRow:eq('+(parseInt(index_installment))+') div.ccCol:eq(2)').html(installment[index_installment].Tenor[index_tenor].Interest+'%');});$('.detailGallery a').not('.bx-clone').magnificPopup({type:'image',tLoading:'Loading image %curr% of %total%',mainClass:'mfp-img-mobile',gallery:{enabled:true,navigateByImgClick:true,preload:[0,1]},image:{tError:'<a href="%url%">The image #%curr%</a> could not be loaded.',titleSrc:function(item){return item.el.attr('title')+'<small>by JakartaNotebook.com</small>';}}});})