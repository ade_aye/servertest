function popup_email_clear_message()
{$('#popup-remind div.notif').removeClass('text-ok text-danger').html('');}
function popup_email_show_message(type,message)
{var icon_class=(type==='success'?'check':'cross');popup_email_clear_message();var color='';if(type=='success')color='text-ok';else if(type=='fail')color='text-danger'
$('#popup-remind div.notif').addClass(color).html('<i class="ir ico-'+icon_class+'"></i>'+message);if(type==='fail')return false;return true;}
function popup_email_finish_submit()
{$('#popup-remind-submit-btn').removeAttr('disabled').removeClass('isDisabled');}
function popup_email_start_submit()
{$('#popup-remind-submit-btn').attr('disabled','disabled').addClass('isDisabled');}
function email_me_reloading_captcha()
{popup_email_start_submit();Captcha.create("popup-remind-captcha-container",function()
{popup_email_finish_submit();if($('#popup-remind-email').val()!=='')$('#popup-remind input[name="captcha_response"]').focus();else $('#popup-remind-email').focus();});return false;}
function email_me(item_id,item_name,branch_id,disabled_branches)
{popup_email_clear_message();$('#popup-remind-product-name').html(item_name);$('#popup-remind input[name="branch[]"]').removeAttr('disabled');$('#popup-remind input[name="id"]').val(item_id);$('#popup-remind input[name="branch[]"][value="'+branch_id+'"]').prop('checked',true);$('#popup-remind input[name="branch[]"][value!="'+branch_id+'"]').prop('checked',false);for(i=0;i<disabled_branches.length;i++)
{$('#popup-remind input[name="branch[]"][value="'+disabled_branches[i]+'"]').attr('disabled','disabled');$('#popup-remind input[name="branch[]"][value="'+disabled_branches[i]+'"] + label').attr('style','text-decoration:line-through; color: #aaa');$('#popup-remind input[name="branch[]"][value="'+disabled_branches[i]+'"]').parent().hide();$('#popup-remind-form').show();}
email_me_reloading_captcha();return true;}
$(function(){$('a.popup-remind').magnificPopup({type:'inline'});$('#popup-remind-form').ajaxForm({beforeSubmit:function(arr,form,options){if($('#popup-remind-email').val()==='')
{$('#popup-remind-email').focus();return popup_email_show_message('fail','Anda belum mengisi email.');}
else if($('#popup-remind input[name="branch[]"]:checked').length===0)
{return popup_email_show_message('fail','Minimal satu cabang harus dipilih.');}
else if($('#popup-remind input[name="captcha_response"]').val()==='')
{$('#popup-remind input[name="captcha_response"]').focus();return popup_email_show_message('fail','Anda belum mengisi captcha.');}
else if($('#popup-remind input[name="captcha_response"]').val().length!=$('#popup-remind input[name="captcha_response"]').attr('maxlength'))
{$('#popup-remind input[name="captcha_response"]').focus();return popup_email_show_message('fail','Jumlah huruf captcha tidak sesuai.');}
popup_email_start_submit();return true;},clearForm:false,dataType:'json',error:function(){popup_email_show_message('fail','Maaf terjadi kesalahan fatal pada system kami, silahkan coba lagi beberapa saat lagi.');popup_email_finish_submit();email_me_reloading_captcha();},resetForm:false,success:function(result){if(result!==null){if(result.success){popup_email_show_message('success',result.message);}
else
{popup_email_show_message('fail',result.message);email_me_reloading_captcha();}}
else
{popup_email_show_message('fail','Maaf terjadi kesalahan fatal pada sistem kami, silahkan coba lagi beberapa saat lagi.');email_me_reloading_captcha();}
popup_email_finish_submit();}});});