window.hjSiteSettings = window.hjSiteSettings || {"testers_widgets":[],"surveys":[],"heatmaps":[{"created_epoch_time":1467391719,"targeting":[{"negate":false,"pattern":"https:\/\/www.jakartanotebook.com\/idul-fitri-1437?utm_source=sendy","match_operation":"starts_with","component":"url"}],"id":621114}],"recording_capture_keystrokes":true,"polls":[],"site_id":228920,"record_targeting_rules":[{"negate":false,"pattern":"https:\/\/www.jakartanotebook.com\/","match_operation":"simple","component":"url"}],"forms":[{"field_info":[{"field_type":"text","match_value":"fullname","id":447882,"match_attribute":"name"},{"field_type":"email","match_value":"email","id":447883,"match_attribute":"name"},{"field_type":"tel","match_value":"phone","id":447884,"match_attribute":"name"},{"field_type":"password","match_value":"password","id":447885,"match_attribute":"name"},{"field_type":"text","match_value":"captcha_response","id":447886,"match_attribute":"name"},{"field_type":"checkbox","match_value":"subscribe","id":447887,"match_attribute":"name"}],"targeting":[{"negate":false,"pattern":"https:\/\/www.jakartanotebook.com\/member\/register","match_operation":"simple","component":"url"}],"selector_type":"css","created_epoch_time":1465537328,"selector":"0:html > body.page-body > div > div.content__body > div.member__col > div.container500 > div.box.container > div.pad > form.form","id":44939}],"record":false,"r":0.1122511599,"deferred_page_contents":[]};

window.hjBootstrap = window.hjBootstrap || function (scriptUrl) {
    var s = document.createElement('script');
    s.src = scriptUrl;
    document.getElementsByTagName('head')[0].appendChild(s);
    window.hjBootstrap = function() {};
};

hjBootstrap('https://script.hotjar.com/modules-ff53d42a4d7f100201c6e78983209c2f.js');